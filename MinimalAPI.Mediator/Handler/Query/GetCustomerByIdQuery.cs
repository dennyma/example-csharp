using MinimalAPI.Mediator.Services;

namespace MinimalAPI.Mediator.Handler.Query;

public record GetCustomerByIdQuery(Guid Id): IRequest<IResult>;

public class GetCustomerByIdQueryHandler : IRequestHandler<GetCustomerByIdQuery, IResult>
{
	private readonly ICustomerService _service;

	public GetCustomerByIdQueryHandler(ICustomerService service)
	{
		_service = service;
	}
	
	public Task<IResult> Handle(GetCustomerByIdQuery request, CancellationToken cancellationToken)
	{
		var customer = _service.GetById(request.Id);
		return Task.FromResult(customer is not null ? Results.Ok(customer) : Results.NotFound());
	}
}