using MinimalAPI.Mediator.Services;

namespace MinimalAPI.Mediator.Handler.Query;

public record GetAllCustomersQuery: IRequest<List<Customer>>;

public class GetAllCustomersQueryHandler : IRequestHandler<GetAllCustomersQuery, List<Customer>>
{
	private readonly ICustomerService _service;

	public GetAllCustomersQueryHandler(ICustomerService service)
	{
		_service = service;
	}
	
	public Task<List<Customer>> Handle(GetAllCustomersQuery request, CancellationToken cancellationToken)
	{
		return Task.FromResult(_service.GetAll());
	}
}