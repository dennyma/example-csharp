using MinimalAPI.Mediator.Services;

namespace MinimalAPI.Mediator.Handler.Command;

public record UpdateCustomerCommand(Guid Id, Customer UpdatedCustomer): IRequest<IResult>;

public class UpdateCustomerCommandHandler : IRequestHandler<UpdateCustomerCommand, IResult>
{
	private readonly ICustomerService _service;

	public UpdateCustomerCommandHandler(ICustomerService service)
	{
		_service = service;
	}
	
	public Task<IResult> Handle(UpdateCustomerCommand request, CancellationToken cancellationToken)
	{
		var customer = _service.GetById(request.Id);
		if (customer is null)
		{
			return Task.FromResult(Results.NotFound());
		}

		_service.Update(request.UpdatedCustomer);
		return Task.FromResult(Results.Ok(request.UpdatedCustomer));
	}
}