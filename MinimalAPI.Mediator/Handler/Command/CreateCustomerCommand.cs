using MinimalAPI.Mediator.Services;

namespace MinimalAPI.Mediator.Handler.Command;

public record CreateCustomerCommand(Customer Customer): IRequest<IResult>;

public class CreateCustomerCommandHandler : IRequestHandler<CreateCustomerCommand, IResult>
{
	private readonly ICustomerService _service;

	public CreateCustomerCommandHandler(ICustomerService service)
	{
		_service = service;
	}
	
	public Task<IResult> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
	{
		_service.Create(request.Customer);
		return Task.FromResult(Results.Created($"/customers/{request.Customer.Id}", request.Customer));
	}
}