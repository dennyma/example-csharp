using MinimalAPI.Mediator.Services;

namespace MinimalAPI.Mediator.Handler.Command;

public record DeleteCustomerCommand(Guid Id): IRequest<IResult>;

public class DeleteCustomerCommandHandler : IRequestHandler<DeleteCustomerCommand, IResult>
{
	private readonly ICustomerService _service;

	public DeleteCustomerCommandHandler(ICustomerService service)
	{
		_service = service;
	}
	
	public Task<IResult> Handle(DeleteCustomerCommand request, CancellationToken cancellationToken)
	{
		_service.Delete(request.Id);
		return Task.FromResult(Results.Ok());
	}
}