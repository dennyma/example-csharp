using FluentValidation;
using MinimalAPI.Mediator;
using MinimalAPI.Mediator.Endpoints;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddMediatR(typeof(IAssemblyMarker));
builder.Services.AddCustomerServices();
builder.Services.AddSwaggerServices();
builder.Services.AddValidatorsFromAssemblyContaining(typeof(IAssemblyMarker));

var app = builder.Build();

app.MapCustomerEndpoints();
app.MapSwaggerEndpoints();

app.Run();