using FluentValidation;
using MinimalAPI.Mediator.Handler.Command;
using MinimalAPI.Mediator.Handler.Query;
using MinimalAPI.Mediator.Services;

namespace MinimalAPI.Mediator.Endpoints;

public static class CustomerEndpoints
{
    public static void MapCustomerEndpoints(this WebApplication app)
    {
        app.MapGet("/customers", GetAllCustomers);
        app.MapGet("/customers/{id}", GetCustomerById);
        app.MapPost("/customers", CreateCustomer).WithValidator<Customer>();
        app.MapPut("/customers/{id}", UpdateCustomer);
        app.MapDelete("/customers/{id}", DeleteCustomerById);
    }

    public static void AddCustomerServices(this IServiceCollection services)
    {
        services.AddSingleton<ICustomerService, CustomerService>();
    }

    internal static async Task<List<Customer>> GetAllCustomers(IMediator mediator)
    {
        return await mediator.Send(new GetAllCustomersQuery());
    }

    internal static async Task<IResult> GetCustomerById(IMediator mediator, Guid id)
    {
        return await mediator.Send(new GetCustomerByIdQuery(id));
    }

    internal static async Task<IResult> CreateCustomerWithValidation(
        IMediator mediator, Customer customer,
        IValidator<Customer> validator)
    {
        var validate = await validator.ValidateAsync(customer);
        if (!validate.IsValid)
        {
            return Results.BadRequest(validate.Errors);
        }

        return await mediator.Send(new CreateCustomerCommand(customer));
    }

    internal static async Task<IResult> CreateCustomer(
        IMediator mediator, Customer customer)
    {
        return await mediator.Send(new CreateCustomerCommand(customer));
    }

    internal static async Task<IResult> UpdateCustomer(IMediator mediator, Guid id, Customer updatedCustomer)
    {
        return await mediator.Send(new UpdateCustomerCommand(id, updatedCustomer));
    }

    internal static async Task<IResult> DeleteCustomerById(IMediator mediator, Guid id)
    {
        return await mediator.Send(new DeleteCustomerCommand(id));
    }
}