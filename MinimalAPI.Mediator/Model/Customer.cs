namespace MinimalAPI.Mediator.Model;

public class Customer
{
	public Guid Id { get; } = Guid.NewGuid();

	public string FullName { get; init; } = default!;
}