using Serilog;
using Serilog.Events;
using ILogger = Serilog.ILogger;

namespace MinimalAPI;

public static class SerilogExtensions
{
	public static ILogger AddSerilog(this LoggerConfiguration loggerConfiguration)
	{
		return loggerConfiguration.Enrich.FromLogContext()
			.WriteTo.Console(LogEventLevel.Information, "[{Timestamp:HH:mm:ss} {Level:u3} {SourceContext}] {Message}{NewLine}{Exception}")
			.Filter.ByExcluding(c => c.Properties.Any(p => p.Value.ToString().Contains("swagger")))
			.MinimumLevel.Debug()
			.MinimumLevel.Override("Microsoft", LogEventLevel.Information)
			.MinimumLevel.Override("Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker", LogEventLevel.Information)
			.MinimumLevel.Override("System", LogEventLevel.Warning).CreateLogger();
	}
}