namespace MinimalAPI.Endpoints;

public static class SwaggerEndpoints
{
	public static void MapSwaggerEndpoints(this WebApplication app)
	{
		if (app.Environment.IsDevelopment())
		{
			app.UseSwagger();
			app.UseSwaggerUI();
		}
	}
	
	public static void AddSwaggerServices(this IServiceCollection services)
	{
		services.AddSwaggerGen();
		services.AddEndpointsApiExplorer();
	}
}