using FluentValidation;
using MinimalAPI.Model;

namespace MinimalAPI.Validator;

public class CustomerValidation : AbstractValidator<Customer>
{
	public CustomerValidation()
	{
		RuleFor(x => x.Id).NotEmpty();
		RuleFor(x => x.FullName).NotEmpty();
	}
}