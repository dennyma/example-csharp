using FluentValidation;
using MinimalAPI;
using MinimalAPI.Endpoints;
using Serilog;

var builder = WebApplication.CreateBuilder(args);
Log.Logger = new LoggerConfiguration().AddSerilog();

builder.WebHost.UseSerilog();
builder.Services.AddCustomerServices();
builder.Services.AddLogging();
builder.Services.AddSwaggerServices();
builder.Services.AddAuthorization();
builder.Services.AddValidatorsFromAssemblyContaining(typeof(IAssemblyMarker));

var app = builder.Build();
app.MapSwaggerEndpoints();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapCustomerEndpoints();

app.Run();