using System;

namespace TestProject.Requests;

public class RoomResponse
{
	public Guid Id { get; init; }
	public string RoomName { get; init; }
}