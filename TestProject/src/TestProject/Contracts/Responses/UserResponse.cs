using System;

namespace TestProject.Requests;

public class UserResponse
{
	public Guid Id { get; init; }

	public string Username { get; init; }
}