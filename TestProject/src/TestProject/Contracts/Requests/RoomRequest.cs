using System;

namespace TestProject.Requests;

public class RoomRequest
{
	public Guid UserId { get; set; }
	public string RoomName { get; set; }
}