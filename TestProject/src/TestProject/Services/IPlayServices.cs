using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Models;

namespace TestProject.Services;

public interface IPlayServices
{
	Task<List<Room>> GetAllRoomsAsync();
	Task<List<User>> GetAllUsersAsync();
	Task<User> GetUserByIdAsync(Guid id);
	Task<bool> CreateRoomAsync(Guid userId, Room room);
	Task<bool> CreateUserAsync(User user);
}