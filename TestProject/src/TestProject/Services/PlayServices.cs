using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using TestProject.Models;
using TestProject.Repositories;

namespace TestProject.Services;

public class PlayServices: IPlayServices
{
	private IRoomRepository RoomRepository { get; }
	private IUserRepository UserRepository { get; }
	private ILogger<PlayServices> Logger { get; }

	public PlayServices(IRoomRepository roomRepository, ILogger<PlayServices> logger, IUserRepository userRepository)
	{
		RoomRepository = roomRepository;
		Logger = logger;
		UserRepository = userRepository;
	}
	
	public async Task<List<Room>> GetAllRoomsAsync()
	{
		Logger.LogInformation("Retrieving all rooms");
		var stopWatch = Stopwatch.StartNew();
		try
		{
			return await RoomRepository.GetAllAsync();
		}
		catch (Exception e)
		{
			Logger.LogError(e, "Something went wrong while retrieving all rooms");
			throw;
		}
		finally
		{
			stopWatch.Stop();
			Logger.LogInformation("All rooms retrieved in {time}ms", stopWatch.ElapsedMilliseconds);
		}

	}
	
	public async Task<List<User>> GetAllUsersAsync()
	{
		Logger.LogInformation("Retrieving all users");
		var stopWatch = Stopwatch.StartNew();
		try
		{
			return await UserRepository.GetAllAsync();
		}
		catch (Exception e)
		{
			Logger.LogError(e, "Something went wrong while retrieving all users");
			throw;
		}
		finally
		{
			stopWatch.Stop();
			Logger.LogInformation("All users retrieved in {time}ms", stopWatch.ElapsedMilliseconds);
		}
	}

	public async Task<User> GetUserByIdAsync(Guid id)
	{
		Logger.LogInformation("Retrieving user with {id}", id);
		var stopWatch = Stopwatch.StartNew();
		try
		{
			return await UserRepository.GetByIdAsync(id);
		}
		catch (Exception e)
		{
			Logger.LogError(e, "Something went wrong while retrieving user with {id}", id);
			throw;
		}
		finally
		{
			stopWatch.Stop();
			Logger.LogInformation("User with id {id} retrieved in {time}ms", id, stopWatch.ElapsedMilliseconds);
		}
	}

	public async Task<bool> CreateUserAsync(User user)
	{
		Logger.LogInformation("Creating user with id {id} and username {username}", user.Id, user.Username);
		
		var stopWatch = Stopwatch.StartNew();
		try
		{
			var users = await UserRepository.GetAllAsync();

			if (users.Find(x => x.Username == user.Username) is not null)
			{
				Logger.LogInformation("Creating user failed because username {username} exists", user.Username);
				return false;
			}

			await UserRepository.CreateAsync(user);
			return true;
		}
		catch (Exception e)
		{
			Logger.LogError(e, "Something went wrong while creating user");
			throw;
		}
		finally
		{
			stopWatch.Stop();
			Logger.LogInformation("Create User with id {id} in {time}ms", user.Id, stopWatch.ElapsedMilliseconds);
		}
	}
	
	public async Task<bool> CreateRoomAsync(Guid userId, Room room)
	{
		Logger.LogInformation("Creating room with id {roomId} and name {roomName} by user with id {userId}", room.Id, room.RoomName, userId);
		var stopWatch = Stopwatch.StartNew();
		try
		{
			var user = await UserRepository.GetByIdAsync(userId);
			if (user is null)
			{
				Logger.LogInformation("Creating room with failed because user not found");
				return false;
			}

			var result = await RoomRepository.CreateAsync(userId, room);

			if (!result)
			{
				Logger.LogInformation("Create room failed because user already in a room");
			}

			return result;
		}
		catch (Exception e)
		{
			Logger.LogError(e, "Something went wrong while creating room");
			throw;
		}
		finally
		{
			stopWatch.Stop();
			Logger.LogInformation("Create room with id {roomId} in {time}ms", room.Id, stopWatch.ElapsedMilliseconds);
		}
	}
}