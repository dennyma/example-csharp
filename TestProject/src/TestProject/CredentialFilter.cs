using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace TestProject;

public class CredentialFilter: IAsyncActionFilter
{
	private readonly ILogger<CredentialFilter> _logger;
		
	public CredentialFilter(ILogger<CredentialFilter> logger)
	{
		_logger = logger;
	}
		
	public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
	{
		// Before
		var remote = $"{context.HttpContext.Connection.RemoteIpAddress}:{context.HttpContext.Connection.RemotePort}";
		_logger.LogInformation("Remote Address: {remote}", remote);
			
		if (context.ActionDescriptor is ControllerActionDescriptor controller)
		{
			var attribute = controller.MethodInfo.GetCustomAttributes(true);
			bool anonymous = false;
			for (int i = 0; i < attribute.Length; i++)
			{
				if (attribute[i] is AllowAnonymousAttribute)
				{
					anonymous = true;
					break;
				}
			}

			_logger.LogInformation("Method Allow Anonymous: {anonymous}", anonymous);
			if (!anonymous)
			{
				var id = System.Convert.ToInt32(context.HttpContext.Request.Headers["id"]);
				var key = System.Convert.ToInt32(context.HttpContext.Request.Headers["key"]);
					
				_logger.LogInformation("id: {id}, key {key}", id, key);
				if (key != 1234567)
				{
					var result = new { message = "Invalid Credential" };
					context.Result = new UnauthorizedObjectResult(result);
					return;
				}
			}
		}
			
		await next();
			
		// After
	}
}