using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using TestProject.Mappers;
using TestProject.Models;
using TestProject.Services;

namespace TestProject.Handler;

public record CreateUserCommand(string Username) : IRequest<IActionResult>;
	
public class CreateUserCommandHandler: IRequestHandler<CreateUserCommand, IActionResult>
{
	private readonly IPlayServices _services;

	public CreateUserCommandHandler(IPlayServices services)
	{
		_services = services;
	}

	public async Task<IActionResult> Handle(CreateUserCommand request, CancellationToken cancellationToken)
	{
		var user = new User
		{
			Id = Guid.NewGuid(),
			Username = request.Username
		};

		var success = await _services.CreateUserAsync(user);

		return success ? new OkObjectResult(user.ToUserResponse()) : new BadRequestResult();
	}
}