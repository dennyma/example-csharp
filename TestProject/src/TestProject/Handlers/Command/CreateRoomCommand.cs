using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using TestProject.Models;
using TestProject.Services;

namespace TestProject.Handler.Command;

public record CreateRoomCommand(string RoomName, Guid UserId) : IRequest<IActionResult>;
	
public class CreateRoomCommandHandler: IRequestHandler<CreateRoomCommand, IActionResult>
{
	private readonly IPlayServices _services;

	public CreateRoomCommandHandler(IPlayServices services)
	{
		_services = services;
	}

	public async Task<IActionResult> Handle(CreateRoomCommand request, CancellationToken cancellationToken)
	{
		var room = new Room
		{
			Id = Guid.NewGuid(),
			RoomName = request.RoomName
		};
		
		var success = await _services.CreateRoomAsync(request.UserId, room);
		IActionResult result = success ? new OkResult() : new BadRequestResult();

		return result;
	}
}