using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using TestProject.Mappers;
using TestProject.Models;
using TestProject.Services;

namespace TestProject.Handler;

public record GetAllUsersQuery : IRequest<IActionResult>;
	
public class GetAllUsersQueryHandler: IRequestHandler<GetAllUsersQuery, IActionResult>
{
	private readonly IPlayServices _services;

	public GetAllUsersQueryHandler(IPlayServices services)
	{
		_services = services;
	}

	public async Task<IActionResult> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
	{
		var result = await _services.GetAllUsersAsync();
		
		return new OkObjectResult(result.Select(x => x.ToUserResponse()));
	}
}