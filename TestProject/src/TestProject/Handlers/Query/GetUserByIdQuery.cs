using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using TestProject.Mappers;
using TestProject.Models;
using TestProject.Services;

namespace TestProject.Handler;

public record GetUserByIdQuery(Guid Id) : IRequest<IActionResult>;
	
public class GetUserByIdQueryHandler: IRequestHandler<GetUserByIdQuery, IActionResult>
{
	private readonly IPlayServices _services;

	public GetUserByIdQueryHandler(IPlayServices services)
	{
		_services = services;
	}

	public async Task<IActionResult> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
	{
		var result = await _services.GetUserByIdAsync(request.Id);

		if (result is null)
		{
			return new NotFoundResult();
		}
		
		return new OkObjectResult(result.ToUserResponse());
	}
}