using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using TestProject.Mappers;
using TestProject.Services;

namespace TestProject.Handler.Query;
public record GetAllRoomsQuery : IRequest<IActionResult>;

public class GetAllRoomsQueryHandler: IRequestHandler<GetAllRoomsQuery, IActionResult>
{
	private readonly IPlayServices _services;

	public GetAllRoomsQueryHandler(IPlayServices services)
	{
		_services = services;
	}

	public async Task<IActionResult> Handle(GetAllRoomsQuery request, CancellationToken cancellationToken)
	{
		var result = await _services.GetAllRoomsAsync();
		
		return new OkObjectResult(result.Select(x => x.ToUserResponse()));
	}
}
