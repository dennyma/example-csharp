using System.Data.Common;
using MySqlConnector;

namespace TestProject;

public interface IDbConnectionFactory
{
	DbConnection NewConnection();
}

public class MysqlConnectionFactory : IDbConnectionFactory
{
	private readonly string _connectionString;

	public MysqlConnectionFactory(string connectionString)
	{
		_connectionString = connectionString;
	}

	public DbConnection NewConnection()
	{
		return new MySqlConnection(_connectionString);
	}
}