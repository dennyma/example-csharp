using TestProject.Models;
using TestProject.Requests;

namespace TestProject.Mappers;

public static class UserMapper
{
	public static UserResponse ToUserResponse(this User user)
	{
		return new UserResponse
		{
			Id = user.Id,
			Username = user.Username
		};
	}
}

public static class RoomMapper
{
	public static RoomResponse ToUserResponse(this Room room)
	{
		return new RoomResponse
		{
			Id = room.Id,
			RoomName = room.RoomName
		};
	}
}