using System;

namespace TestProject.Models;

public record User
{
	public Guid Id { get; init; }

	public string Username { get; init; }
}