using System;

namespace TestProject.Models;

public record Room
{
	public Guid Id { get; init; }
	public string RoomName { get; init; }
}