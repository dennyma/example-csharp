using FluentValidation;
using TestProject.Requests;

namespace TestProject.Validator;

public class RoomRequestValidator: AbstractValidator<RoomRequest>
{
	public RoomRequestValidator()
	{
		RuleFor(x => x.RoomName)
			.NotEmpty()
			.WithErrorCode("100")
			.WithMessage("{PropertyName} cannot be empty")
			.NotNull()
			.WithErrorCode("101")
			.WithMessage("{PropertyName} cannot be null")
			.WithName("room_name");
	}
}