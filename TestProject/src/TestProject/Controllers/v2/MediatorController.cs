using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestProject.Handler;
using TestProject.Models;

namespace TestProject.Controllers.v2;

[ApiController]
[Route("api/[controller]")]
public class MediatorController: ControllerBase
{
	private readonly IMediator _mediator;
	
	public MediatorController(IMediator mediator)
	{
		_mediator = mediator;
	}

	[HttpGet, AllowAnonymous]
	public async Task<IActionResult> GetUser()
	{
		var query = new GetAllUsersQuery();
		var result = await _mediator.Send(query);
		return result;
	}
}