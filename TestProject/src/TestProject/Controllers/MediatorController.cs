﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestProject.Handler;
using TestProject.Handler.Command;
using TestProject.Handler.Query;
using TestProject.Models;
using TestProject.Requests;

namespace TestProject.Controllers;

[ApiController]
[Route("api/[controller]")]
public class MediatorController : ControllerBase
{
    private readonly IMediator _mediator;

    public MediatorController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet, AllowAnonymous]
    public IActionResult GetServerTime()
    {
        return Ok(DateTimeOffset.UtcNow.ToUnixTimeSeconds());
    }
        
    [HttpGet("User")]
    public async Task<IActionResult> GetUser()
    {
        var query = new GetAllUsersQuery();
        var result = await _mediator.Send(query);
        return result;
    }
    
    [HttpGet("User/{id:guid}")]
    public async Task<IActionResult> GetUserById(Guid id)
    {
        var query = new GetUserByIdQuery(id);
        var result = await _mediator.Send(query);

        return result;
    }
        
    [HttpGet("Room")]
    public async Task<IActionResult> GetRoom()
    {
        var query = new GetAllRoomsQuery();
        var result = await _mediator.Send(query);
        return result;
    }
        
    [HttpPost("Room")]
    public async Task<IActionResult> CreateRoom([FromForm] RoomRequest request)
    {
        var query = new CreateRoomCommand(request.RoomName, request.UserId);
        var result = await _mediator.Send(query);
        return result;
    }
        
    [HttpPost("User")]
    public async Task<IActionResult> CreateUser([FromForm] string username)
    {
        var command = new CreateUserCommand(username);
        var result = await _mediator.Send(command);
        
        return result;
    }
}