using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestProject.Models;
using TestProject.Requests;
using TestProject.Validator;

namespace TestProject.Controllers;

[ApiController]
[Route("api/[controller]")]
public class VanillaController: ControllerBase
{
	private IDbConnectionFactory DbConnectionFactory { get; }
		
	public VanillaController(IDbConnectionFactory dbConnectionFactory)
	{
		DbConnectionFactory = dbConnectionFactory;
	}
		
	[HttpGet]
	public async Task<ActionResult<List<User>>> GetUser([FromHeader(Name = "id")] int id,
		[FromHeader(Name = "key")] int key)
	{
		List<User> result = new();

		using DbConnection connection = DbConnectionFactory.NewConnection();

		await connection.OpenAsync();

		using DbCommand command = connection.CreateCommand();

		command.CommandText = 
			"SELECT t.id, t.username FROM lokapala_accountdb.t_user t;";

		using DbDataReader reader = await command.ExecuteReaderAsync();
		while (await reader.ReadAsync())
		{
			result.Add(new()
			{
				Id = reader.GetGuid(0),
				Username = reader.GetString(1)
			});
		}
			
		return result;
	}
		
	[AllowAnonymous]
	[HttpGet("Dapper")]
	public async Task<ActionResult<List<User>>> GetUserDapper([FromForm] RoomRequest request)
	{
		var validator = await new RoomRequestValidator().ValidateAsync(request);
		if (!validator.IsValid)
		{
			var error = validator.Errors[0];
			var errorMessage = new { message = error.ErrorMessage, code = error.ErrorCode };
			return Ok(errorMessage);
		}
			
		using DbConnection connection = DbConnectionFactory.NewConnection();

		await connection.OpenAsync();

		using DbTransaction transaction = await connection.BeginTransactionAsync();
			
		var parameter = new { id = 1 };
			
		var result = 
			await connection.QueryAsync<User>(
				"SELECT t.id, t.username FROM lokapala_accountdb.t_user t WHERE t.id = @id", 
				parameter, 
				transaction);
			
		return new List<User>(result);
	}
}