using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Models;

namespace TestProject.Repositories;

public class UserRepository : IUserRepository
{
	private static readonly ConcurrentDictionary<Guid, User> Users = new();

	public Task<List<User>> GetAllAsync()
	{
		var result = new List<User>(Users.Values);
		return Task.FromResult(result);
	}

	public Task<User> GetByIdAsync(Guid id)
	{
		Users.TryGetValue(id, out var user);
		return Task.FromResult(user);
	}

	public Task CreateAsync(User user)
	{
		return Task.FromResult(Users.TryAdd(user.Id, user));
	}
}