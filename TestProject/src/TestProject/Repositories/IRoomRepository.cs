using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Models;

namespace TestProject.Repositories;

public interface IRoomRepository
{
	Task<List<Room>> GetAllAsync();
	Task<bool> CreateAsync(Guid userId, Room room);
}