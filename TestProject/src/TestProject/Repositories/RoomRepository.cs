using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Models;

namespace TestProject.Repositories;

public class RoomRepository : IRoomRepository
{
	private static readonly ConcurrentDictionary<Guid, Room> Rooms = new();
	
	public Task<List<Room>> GetAllAsync()
	{
		var rooms = new List<Room>(Rooms.Values);
		
		return Task.FromResult(rooms);
	}
	
	public Task<bool> CreateAsync(Guid userId, Room room)
	{
		return Task.FromResult(Rooms.TryAdd(userId, room));
	}
}