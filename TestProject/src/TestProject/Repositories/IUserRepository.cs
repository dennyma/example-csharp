using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Models;

namespace TestProject.Repositories;

public interface IUserRepository
{
	Task<List<User>> GetAllAsync();
	Task<User> GetByIdAsync(Guid id);
	Task CreateAsync(User user);
}