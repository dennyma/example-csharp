using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using Newtonsoft.Json;
using TestProject.Models;
using TestProject.Requests;
using Xunit;
using Xunit.Extensions.Ordering;
using Xunit.Sdk;

[assembly: TestCaseOrderer("Xunit.Extensions.Ordering.TestCaseOrderer", "Xunit.Extensions.Ordering")]
namespace TestProject.Tests.Integration;

public class CreateUserTests
{

	[Fact]
	public async void CreateUser_ShouldReturnsUnauthorized_WhenInvalidCredential()
	{
		// Arrange
		using var app = new TestApplicationFactory();

		using var httpClient = app.CreateClient();
		
		// Act
		var response = await httpClient.PostAsync($"/api/mediator/user", null);

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
	}

	[Fact, Order(1)]
	public async Task CreateUser_ShouldReturnsUser_WhenUserSuccessCreated()
	{
		// Arrange
		var user = new User
		{
			Username = "Denny Marco"
		};
		
		using var app = new TestApplicationFactory();
			
		using var httpClient = app.CreateClient();
		httpClient.DefaultRequestHeaders.Add("key", "1234567");

		// Act
		using var createMessage = new HttpRequestMessage(HttpMethod.Post, "/api/mediator/User");
		createMessage.Content = new FormUrlEncodedContent(new []
		{
			new KeyValuePair<string, string>("username", user.Username)
		});
		var createResponse = await httpClient.SendAsync(createMessage);
		var createResponseString = await createResponse.Content.ReadAsStringAsync();
		var createResult = JsonConvert.DeserializeObject<UserResponse>(createResponseString);
			
		var getResponse = await httpClient.GetAsync($"api/Mediator/User/{createResult.Id}");
		var getResponseString = await getResponse.Content.ReadAsStringAsync();
		var getResult = JsonConvert.DeserializeObject<UserResponse>(getResponseString);
		
		// Assert
		createResponse.StatusCode.Should().Be(HttpStatusCode.OK);
		createResult.Username.Should().Be(user.Username);
		createResult.Id.Should().NotBeEmpty();

		getResponse.StatusCode.Should().Be(HttpStatusCode.OK);
		getResult.Should().BeEquivalentTo(createResult);
	}
	
	[Fact, Order(2)]
	public async void CreateUser_ShouldReturnsBadRequest_WhenUserDuplicated()
	{
		// Arrange
		var user = new User
		{
			Username = "Denny Marco"
		};
		
		using var app = new TestApplicationFactory();

		using var httpClient = app.CreateClient();
		httpClient.DefaultRequestHeaders.Add("key", "1234567");

		// Act
		using var duplicateMessage = new HttpRequestMessage(HttpMethod.Post, "/api/mediator/User");
		duplicateMessage.Content = new FormUrlEncodedContent(new []
		{
			new KeyValuePair<string, string>("username", user.Username)
		});
		var duplicateResponse = await httpClient.SendAsync(duplicateMessage);

		// Assert
		duplicateResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
	}
}