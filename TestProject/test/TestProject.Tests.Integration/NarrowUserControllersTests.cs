using System;
using System.Net;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NSubstitute;
using TestProject.Models;
using TestProject.Repositories;
using TestProject.Requests;
using TestProject.Services;
using Xunit;
using Xunit.Abstractions;

namespace TestProject.Tests.Integration;

public class NarrowUserControllersTests
{
	private readonly IPlayServices _sut = Substitute.For<IPlayServices>();
	private readonly ITestOutputHelper _testOutput;

	public NarrowUserControllersTests(ITestOutputHelper testOutput)
	{
		_testOutput = testOutput;
	}

	[Fact]
	public async void GetUserById_ShouldReturnsUnauthorized_WhenInvalidCredential()
	{
		// Arrange
		using var app = new TestApplicationFactory();

		var httpClient = app.CreateClient();
		
		// Act
		var response = await httpClient.GetAsync($"/api/mediator/user/{Guid.NewGuid()}");

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
	}

	[Fact]
	public async void GetUserById_ShouldReturnsUser_WhenUserExists()
	{
		// Arrange
		var id = Guid.NewGuid();
		var user = new User
		{
			Id = id,
			Username = "Denny Marco"
		};
		
		_sut.GetUserByIdAsync(id).Returns(user);

		using var app = new TestApplicationFactory(service =>
		{
			service.AddSingleton(_sut);
		});

		var httpClient = app.CreateClient();
		httpClient.DefaultRequestHeaders.Add("key", "1234567");
		
		// Act
		var response = await httpClient.GetAsync($"/api/mediator/user/{id}");
		var responseText = await response.Content.ReadAsStringAsync();
		var result = JsonConvert.DeserializeObject<UserResponse>(responseText);
		
		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.OK);
		result.Should().BeEquivalentTo(user);
	}
	
	[Fact]
	public async void GetUserById_ShouldReturnsNotFound_WhenUserDoesNotExists()
	{
		// Arrange
		var id = Guid.NewGuid();

		using var app = new TestApplicationFactory();

		var httpClient = app.CreateClient();
		httpClient.DefaultRequestHeaders.Add("key", "1234567");
		
		// Act
		var response = await httpClient.GetAsync($"/api/mediator/user/{id}");

		// Assert
		response.StatusCode.Should().Be(HttpStatusCode.NotFound);
	}
}