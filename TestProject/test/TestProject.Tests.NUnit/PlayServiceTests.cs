using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using TestProject.Models;
using TestProject.Services;

namespace TestProject.Tests.NUnit;

public class PlayServiceTests: IDisposable
{
	private readonly IPlayServices _sut = Substitute.For<IPlayServices>();

	[SetUp]
	public void Setup()
	{
		Console.WriteLine("Setup");
	}
	
	[Test]
	public async Task GetAllUsers_ShouldReturnsEmptyList_WhenNoUsersExists()
	{
		// Arrange
		_sut.GetAllUsersAsync().Returns(new List<User>());

		// Act
		var result = await _sut.GetAllUsersAsync();

		// Assert
		result.Should().BeEmpty();
	}
	
	[Test]
	public async Task GetAllUsers_ShouldReturnsUsers_WhenUsersExists()
	{
		// Arrange
		var id = Guid.NewGuid();
		var user = new User
		{
			Id = id,
			Username = "Denny Marco"
		};
		_sut.GetAllUsersAsync().Returns(new List<User> { user });

		// Act
		var result = await _sut.GetAllUsersAsync();

		// Assert
		result.Should().NotBeEmpty().And.Contain(x => x.Id == id && x.Username == "Denny Marco");
	}
	
	[Test]
	public async Task GetAllRooms_ShouldReturnsEmptyList_WhenNoRoomsExists()
	{
		// Arrange
		_sut.GetAllRoomsAsync().Returns(new List<Room>());

		// Act
		var result = await _sut.GetAllRoomsAsync();

		// Assert
		result.Should().BeEmpty();
	}
	
	[Test]
	public async Task GetAllRooms_ShouldReturnsUsers_WhenRoomsExists()
	{
		// Arrange
		var id = Guid.NewGuid();
		var room = new Room
		{
			Id = id,
			RoomName = "Room 1"
		};
		_sut.GetAllRoomsAsync().Returns(new List<Room> { room });

		// Act
		var result = await _sut.GetAllRoomsAsync();

		// Assert
		result.Should().NotBeEmpty().And.ContainSingle(x => x.Id == id && x.RoomName == "Room 1");
	}

	public void Dispose()
	{
		Console.WriteLine("Dispose");
	}
}